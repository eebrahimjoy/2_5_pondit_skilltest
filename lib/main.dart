import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:skill_test/pages/skill_test_page.dart';

void main() {
  runApp(const SkillTest());
}

class SkillTest extends StatelessWidget {
  const SkillTest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(primaryColor: Colors.white),
      home: const SkillTestPage(),
    );
  }
}
